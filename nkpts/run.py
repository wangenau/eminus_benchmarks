"""Analyze the time scaling depending on the number of k-points."""
import matplotlib.pyplot as plt
import numpy as np

from eminus import Atoms, SCF


npkts = np.arange(1, 16)
times_rks = []
times_uks = []

print('Calculate RKS...')
for n in npkts:
    print(f'Calculating npkts={n}...')
    atoms = Atoms('He', (0, 0, 0), verbose=0, unrestricted=False)
    atoms.kpts.kmesh = (n, 1, 1)
    scf = SCF(atoms, opt={'sd': 10})
    scf.run()
    times_rks.append(scf._opt_log['sd']['time'])

print('Calculate UKS...')
for n in npkts:
    print(f'Calculating npkts={n}...')
    atoms = Atoms('He', (0, 0, 0), verbose=0, unrestricted=True)
    atoms.kpts.kmesh = (n, 1, 1)
    scf = SCF(atoms, opt={'sd': 10})
    scf.run()
    times_uks.append(scf._opt_log['sd']['time'])

fig, ax1 = plt.subplots()
ax1.set_xlabel('Number of k-points')
ax1.set_ylabel('Time (RKS) [$s$]', c='blue')
ax1.plot(npkts, times_rks, label='RKS', c='blue')
ax2 = ax1.twinx()
ax2.set_ylabel('Time (UKS) [$s$]', c='orange')
ax2.plot(npkts, times_uks, label='UKS', c='orange')
plt.title('Time for 10 sd steps')
fig.savefig('results.png')
