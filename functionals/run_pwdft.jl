using PWDFT

const pade_path = joinpath(dirname(pathof(PWDFT)), "..", "pseudopotentials", "pade_gth")
const pbe_path = joinpath(dirname(pathof(PWDFT)), "..", "pseudopotentials", "pbe_gth")
const psp = [[joinpath(pade_path, "Ne-q8.gth")], [joinpath(pbe_path, "Ne-q8.gth")], [joinpath(pbe_path, "Ne-q8.gth")]]
const xc = ["PW", "PBE", "SCAN"]  # There is no TPSS in PWDFT, use SCAN instead

for i = 1:size(xc, 1)
    atoms = Atoms(xyz_file="Ne.xyz", LatVecs=gen_lattice_sc(20.0))
    Ham = Hamiltonian(atoms, psp[i], 30.0, Nspin=1, xcfunc=xc[i])
    @time KS_solve_Emin_PCG!(Ham, NiterMax=13, startingrhoe=:random, verbose=false, etot_conv_thr=1e-7)
end
