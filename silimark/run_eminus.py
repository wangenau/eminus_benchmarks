"""Silimark using eminus."""
import time

from eminus import Cell,  SCF

# Main calculation parameters
atoms_params = {'ecut': 30, 'a': 10.2631, 'kmesh': 2, 'verbose': 4}
scf_params = {'xc': 'spw92', 'etol': 1e-7, 'opt': {'auto': 46}}

# Calculate benzene
atoms = Cell('Si', 'diamond', **atoms_params)
scf = SCF(atoms, **scf_params)
print(f'Grid points: {atoms.Ns}')
start = time.perf_counter()
scf.run(betat=1e-3)
end = time.perf_counter()
print(f'Time: {end - start:.3f} s')
