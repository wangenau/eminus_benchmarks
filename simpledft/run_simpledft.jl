using Base.GC: gc  # We have a memory leak somewhere...
using SimpleDFT


function calculate(atoms::Atoms)
    etot = runSCF(atoms; Nit=2000, etol=1e-12)
    println("Etot($(atoms.atom)) = $(round(etot; digits=12)) Eh")
end


const H_atom = Atoms(["H"], [0.0 0.0 0.0;], 16.0, 16.0, [1.0], [60, 60, 60], [1.0])
@time calculate(H_atom)
gc()

const He_atom = Atoms(["He"], [0.0 0.0 0.0;], 16.0, 16.0, [2.0], [60, 60, 60], [2.0])
@time calculate(He_atom)
gc()

const H2_atom = Atoms(["H", "H"], [0.0 0.0 0.0; 1.4 0.0 0.0], 16.0, 16.0, [1.0 1.0], [60, 60, 60], [2.0])
@time calculate(H2_atom)
gc()
