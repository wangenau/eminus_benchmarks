using PWDFT

const psp_path = joinpath(dirname(pathof(PWDFT)), "..", "pseudopotentials", "pade_gth")
const psp = [joinpath(psp_path, "C-q4.gth"), joinpath(psp_path, "H-q1.gth")]

atoms = Atoms(xyz_file="benzene.xyz", LatVecs=gen_lattice_sc(20.0))
Ham = Hamiltonian(atoms, psp, 30.0, Nspin=1, xcfunc="PW")
println("Grid points: $(prod(Ham.pw.Ns))")
@time KS_solve_Emin_PCG!(Ham, startingrhoe=:random, etot_conv_thr=1e-7)
