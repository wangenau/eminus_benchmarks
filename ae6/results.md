# Results

## eminus

MUE:  -61.257 kcal/mol
MAE:  61.257 kcal/mol
RMSE: 29.933 kcal/mol

## PySCF

MUE:  -67.000 kcal/mol
MAE:  67.000 kcal/mol
RMSE: 34.726 kcal/mol
