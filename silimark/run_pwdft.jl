using PWDFT

const psp_path = joinpath(dirname(pathof(PWDFT)), "..", "pseudopotentials", "pade_gth")
const psp = [joinpath(psp_path, "Si-q4.gth")]

atoms = Atoms(xyz_string_frac=
"""
2

Si  0.0  0.0  0.0
Si  0.25  0.25  0.25
""",
in_bohr=true,
LatVecs=gen_lattice_fcc(10.2631))
Ham = Hamiltonian(atoms, psp, 30.0, Nspin=1, xcfunc="PW", meshk=[2,2,2])
println("Grid points: $(prod(Ham.pw.Ns))")
@time KS_solve_Emin_PCG!(Ham, startingrhoe=:random, etot_conv_thr=1e-7, α_t=1e-3)
