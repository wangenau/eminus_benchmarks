import time

from eminus import Atoms, SCF


def calculate(atoms):
    start = time.perf_counter()
    atoms.s = 60
    etot = SCF(atoms, xc='lda,chachiyo', pot='coulomb', guess='pseudo',
               etol=1e-12, opt={'sd': 2000}).run(betat=1e-5)
    print(f'Etot({atoms.atom}) = {etot:.12f} Eh')
    print(f'{time.perf_counter() - start:.3f} seconds')


H = Atoms(['H'], [[0, 0, 0]], 16, 16, unrestricted=False, verbose='warning')
calculate(H)

He = Atoms(['He'], [[0, 0, 0]], 16, 16, unrestricted=False, verbose='warning')
calculate(He)

H2 = Atoms(['H', 'H'], [[0, 0, 0], [1.4, 0, 0]], 16, 16, unrestricted=False, verbose='warning')
calculate(H2)
