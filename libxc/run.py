"""Compare functional implementation timings."""
import time

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng

from eminus.xc import get_xc
from eminus.extras.libxc import libxc_functional, pyscf_functional

size = 10**np.arange(3, 8)
rng = default_rng()


print('Calculate LDA...')
plt.figure()
for spin in (1, 2):
    eminus = []
    libxc = []
    pyscf = []

    for s in size:
        n = np.abs(rng.standard_normal((spin, s)))

        xc = '7'  # lda_c_vwn
        t = time.perf_counter()
        get_xc(xc, n, spin)
        eminus.append(time.perf_counter() - t)
        t = time.perf_counter()
        libxc_functional(xc, n, spin)
        libxc.append(time.perf_counter() - t)
        t = time.perf_counter()
        pyscf_functional(xc, n, spin)
        pyscf.append(time.perf_counter() - t)

    libxc = np.asarray(libxc)
    plt.plot(size, libxc / eminus, label=f'eminus ({"un" if spin == 2 else ""}restricted)')
    plt.plot(size, libxc / pyscf, label=f'pyscf ({"un" if spin == 2 else ""}restricted)')

plt.xscale('log', base=10)
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Density size')
plt.ylabel(r't$_\mathrm{pylibxc}$ / t$_\mathrm{code}$')
plt.title('LDA (larger = faster than pylibxc)')
plt.legend()
plt.savefig('results_lda.png')


print('Calculate GGA...')
plt.figure()
for spin in (1, 2):
    eminus = []
    libxc = []
    pyscf = []

    for s in size:
        n = np.abs(rng.standard_normal((spin, s)))
        dn = np.stack((n, n, n), axis=2)

        xc = '130'  # gga_c_pbe
        t = time.perf_counter()
        get_xc(xc, n, spin, dn)
        eminus.append(time.perf_counter() - t)
        t = time.perf_counter()
        libxc_functional(xc, n, spin, dn)
        libxc.append(time.perf_counter() - t)
        t = time.perf_counter()
        pyscf_functional(xc, n, spin, dn)
        pyscf.append(time.perf_counter() - t)

    libxc = np.asarray(libxc)
    plt.plot(size, libxc / eminus, label=f'eminus ({"un" if spin == 2 else ""}restricted)')
    plt.plot(size, libxc / pyscf, label=f'pyscf ({"un" if spin == 2 else ""}restricted)')

print('Calculate mGGA...')
plt.xscale('log', base=10)
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Density size')
plt.ylabel(r't$_\mathrm{pylibxc}$ / t$_\mathrm{code}$')
plt.title('GGA (larger = faster than pylibxc)')
plt.legend()
plt.savefig('results_gga.png')


plt.figure()
for spin in (1, 2):
    libxc = []
    pyscf = []

    for s in size:
        n = np.abs(rng.standard_normal((spin, s)))
        dn = np.stack((n, n, n), axis=2)

        xc = '267'  # mgga_c_scan
        t = time.perf_counter()
        libxc_functional(xc, n, spin, dn, n)
        libxc.append(time.perf_counter() - t)
        t = time.perf_counter()
        pyscf_functional(xc, n, spin, dn, n)
        pyscf.append(time.perf_counter() - t)

    libxc = np.asarray(libxc)
    plt.plot(size, libxc / pyscf, label=f'pyscf ({"un" if spin == 2 else ""}restricted)')

plt.xscale('log', base=10)
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Density size')
plt.ylabel(r't$_\mathrm{pylibxc}$ / t$_\mathrm{code}$')
plt.title('meta-GGA (larger = faster than pylibxc)')
plt.legend()
plt.savefig('results_mgga.png')
