"""Calculate various systems using different cgforms."""
import glob

import matplotlib.pyplot as plt
import numpy as np

from eminus import Atoms, SCF, read

# Main calculation parameters
atoms_params = {'ecut': 10, 'a': 10, 'verbose': 2}
scf_params = {'xc': 'spw92', 'etol': 1e-6}

# Collect geometries and cgforms
files = glob.glob('../bh6/*.xyz')
names = ('Fletcher-Reeves', 'Polak-Ribiere', 'Hestenes-Stiefel', 'Dai-Yuan')

res = np.zeros((len(names), len(files)))
print('Without preconditioning (restricted):')
for cgform in range(len(names)):
    for i, f in enumerate(files):
        atom, X = read(f)
        atoms = Atoms(atom, X, **atoms_params)
        scf = SCF(atoms, opt={'sd': 25, 'cg': 500}, **scf_params)
        scf.run(cgform=cgform + 1)
        assert scf.is_converged
        res[cgform, i] += scf._opt_log['cg']['iter']
    print(f'{names[cgform]}: {np.sum(res[cgform]) / len(files):.3f}')

plt.figure()
for cgform in range(len(names)):
    plt.plot(np.arange(len(files)), res[cgform], label=names[cgform])
plt.xlabel('System')
plt.ylabel('Iterations')
plt.title('No preconditioner')
plt.legend()
plt.savefig('results.png')

res = np.zeros((len(names), len(files)))
print('\nWith preconditioning (restricted):')
for cgform in range(len(names)):
    for i, f in enumerate(files):
        atom, X = read(f)
        atoms = Atoms(atom, X, **atoms_params)
        scf = SCF(atoms, opt={'sd': 25, 'pccg': 500}, **scf_params)
        scf.run(cgform=cgform + 1)
        assert scf.is_converged
        res[cgform, i] += scf._opt_log['pccg']['iter']
    print(f'{names[cgform]}: {np.sum(res[cgform]) / len(files):.3f}')

plt.figure()
for cgform in range(len(names)):
    plt.plot(np.arange(len(files)), res[cgform], label=names[cgform])
plt.xlabel('System')
plt.ylabel('Iterations')
plt.title('Preconditioner')
plt.legend()
plt.savefig('results_precon.png')

atoms_params = {'ecut': 5, 'a': 10, 'verbose': 2}

res = np.zeros((len(names), len(files)))
print('\nWithout preconditioning (unrestricted):')
for cgform in range(len(names)):
    for i, f in enumerate(files):
        atom, X = read(f)
        atoms = Atoms(atom, X, **atoms_params, unrestricted=True)
        scf = SCF(atoms, opt={'sd': 25, 'cg': 500}, **scf_params)
        scf.run(cgform=cgform + 1)
        assert scf.is_converged
        res[cgform, i] += scf._opt_log['cg']['iter']
    print(f'{names[cgform]}: {np.sum(res[cgform]) / len(files):.3f}')

plt.figure()
for cgform in range(len(names)):
    plt.plot(np.arange(len(files)), res[cgform], label=names[cgform])
plt.xlabel('System')
plt.ylabel('Iterations')
plt.title('No preconditioner (unrestricted)')
plt.legend()
plt.savefig('results_unres.png')

res = np.zeros((len(names), len(files)))
print('\nWith preconditioning (unrestricted):')
for cgform in range(len(names)):
    for i, f in enumerate(files):
        atom, X = read(f)
        atoms = Atoms(atom, X, **atoms_params, unrestricted=True)
        scf = SCF(atoms, opt={'sd': 25, 'pccg': 500}, **scf_params)
        scf.run(cgform=cgform + 1)
        assert scf.is_converged
        res[cgform, i] += scf._opt_log['pccg']['iter']
    print(f'{names[cgform]}: {np.sum(res[cgform]) / len(files):.3f}')

plt.figure()
for cgform in range(len(names)):
    plt.plot(np.arange(len(files)), res[cgform], label=names[cgform])
plt.xlabel('System')
plt.ylabel('Iterations')
plt.title('Preconditioner (unrestricted)')
plt.legend()
plt.savefig('results_precon_unres.png')
