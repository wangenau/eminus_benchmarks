# Results

## Avg. number of iterations

Without preconditioning (restricted):
Fletcher-Reeves: 43.917
Polak-Ribiere: 48.417
Hestenes-Stiefel: 49.167
Dai-Yuan: 43.583

With preconditioning (restricted):
Fletcher-Reeves: 24.250
Polak-Ribiere: 24.167
Hestenes-Stiefel: 38.000
Dai-Yuan: 65.417

Without preconditioning (unrestricted):
Fletcher-Reeves: 74.333
Polak-Ribiere: 54.333
Hestenes-Stiefel: 48.917
Dai-Yuan: 36.917

With preconditioning (unrestricted):
Fletcher-Reeves: 56.083
Polak-Ribiere: 29.083
Hestenes-Stiefel: 54.750
Dai-Yuan: 65.250
