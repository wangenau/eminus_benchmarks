import time

from simpledft import Atoms, SCF


def calculate(atoms):
    start = time.perf_counter()
    etot = SCF(atoms).run(Nit=2000, etol=1e-12)
    print('Etot({}) = {:.12f} Eh'.format(atoms.atom, etot))
    print('{:.6f} seconds'.format(time.perf_counter() - start))


H = Atoms(['H'], [0, 0, 0], 16, 16, [1], [60, 60, 60], [1])
calculate(H)

He = Atoms(['He'], [0, 0, 0], 16, 16, [2], [60, 60, 60], [2])
calculate(He)

H2 = Atoms(['H', 'H'], [[0, 0, 0], [1.4, 0, 0]], 16, 16, [1, 1], [60, 60, 60], [2])
calculate(H2)
