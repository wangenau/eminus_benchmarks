"""Compare FFT operator implementation timings."""
import time

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng

from eminus import Atoms, config


config.threads = 1

size = list(2**np.arange(4, 9)) + [50, 100, 150, 200, 300]
size = sorted(size)
rng = default_rng()

I = []
J = []
Idag = []
Jdag = []

I_torch = []
J_torch = []
Idag_torch = []
Jdag_torch = []

print('Perform real FFTs...')
for s in size:
    print(f'Calculating s={s}...')
    config.use_torch = False
    atoms = Atoms('Ne', (0, 0, 0), ecut=1)
    atoms.s = s
    atoms.build()
    W_full = rng.standard_normal((len(atoms.G2), atoms.occ.Nstate))
    W_active = rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate))
    t = time.perf_counter()
    atoms.I(W_active)
    I.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag.append(time.perf_counter() - t)

    config.use_torch = True
    atoms = Atoms('Ne', (0, 0, 0), ecut=1)
    atoms.s = s
    atoms.build()
    t = time.perf_counter()
    atoms.I(W_active)
    I_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag_torch.append(time.perf_counter() - t)

I = np.asarray(I)
J = np.asarray(J)
Idag = np.asarray(Idag)
Jdag = np.asarray(Jdag)

plt.figure()
plt.plot(size, I / I_torch, label='I')
plt.plot(size, J / J_torch, label='J')
plt.plot(size, Idag / Idag_torch, label='Idag')
plt.plot(size, Jdag / Jdag_torch, label='Jdag')
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Field size (cbrt)')
plt.ylabel(r't$_\mathrm{Scipy}$ / t$_\mathrm{Torch}$')
plt.title('Operators (larger = faster than SciPy)')
plt.legend()
plt.savefig('results_real.png')


I = []
J = []
Idag = []
Jdag = []

I_torch = []
J_torch = []
Idag_torch = []
Jdag_torch = []

print('Perform complex FFTs...')
for s in size:
    print(f'Calculating s={s}...')
    config.use_torch = False
    atoms = Atoms('Ne', (0, 0, 0), ecut=1)
    atoms.s = s
    atoms.build()
    W_full = rng.standard_normal((len(atoms.G2), atoms.occ.Nstate)) + 1j * rng.standard_normal((len(atoms.G2), atoms.occ.Nstate))
    W_active = rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate)) + 1j * rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate))
    t = time.perf_counter()
    atoms.I(W_active)
    I.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag.append(time.perf_counter() - t)

    config.use_torch = True
    atoms = Atoms('Ne', (0, 0, 0), ecut=1)
    atoms.s = s
    atoms.build()
    t = time.perf_counter()
    atoms.I(W_active)
    I_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag_torch.append(time.perf_counter() - t)

I = np.asarray(I)
J = np.asarray(J)
Idag = np.asarray(Idag)
Jdag = np.asarray(Jdag)

plt.figure()
plt.plot(size, I / I_torch, label='I')
plt.plot(size, J / J_torch, label='J')
plt.plot(size, Idag / Idag_torch, label='Idag')
plt.plot(size, Jdag / Jdag_torch, label='Jdag')
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Field size (cbrt)')
plt.ylabel(r't$_\mathrm{Scipy}$ / t$_\mathrm{Torch}$')
plt.title('Operators (larger = faster than SciPy)')
plt.legend()
plt.savefig('results_complex.png')
