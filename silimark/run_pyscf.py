"""Silimark using PySCF."""
import time

import numpy as np

from pyscf.pbc import dft, gto

# Main calculation parameters
atoms_params = {'mesh': [40] * 3, 'basis': 'gth-tzvp', 'a': 10.2631, 'kmesh': [2] * 3, 'verbose': 4}
scf_params = {'xc': 'spw92', 'conv_tol': 1e-7, 'max_cycle': 250}

# Calculate benzene
mol = gto.Cell()
mol.unit = 'bohr'
mol.atom = f'''
Si 0 0 0
Si {0.25 * atoms_params['a']} {0.25 * atoms_params['a']} {0.25 * atoms_params['a']}
'''
mol.a = np.array([[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]]) * atoms_params['a']
mol.mesh = atoms_params['mesh']
mol.basis = atoms_params['basis']
mol.pseudo = 'gth-pade'
mol.verbose = atoms_params['verbose']
mol.build()
kpts = mol.make_kpts(atoms_params['kmesh'])
mf = dft.KRKS(mol, xc=scf_params['xc'], kpts=kpts)
mf.conv_tol = scf_params['conv_tol']
mf.max_cycle = scf_params['max_cycle']
print(f'Grid points: {len(mf.grids.coords)}')
start = time.perf_counter()
mf.run()
end = time.perf_counter()
print(f'Time: {end - start:.3f} s')
