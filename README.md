# eminus benchmarks

A small collection of quantum chemical benchmarks for [eminus](https://wangenau.gitlab.io/eminus) with various purposes.
