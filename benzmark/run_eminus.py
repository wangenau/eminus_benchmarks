"""Benzmark using eminus."""
import time

from eminus import Atoms, read, SCF

# Main calculation parameters
atoms_params = {'ecut': 30, 'a': 20, 'unrestricted': False, 'verbose': 4}
scf_params = {'xc': 'spw92', 'etol': 1e-7, 'opt': {'auto': 33}}

# Calculate benzene
atoms = Atoms(*read('benzene.xyz'), **atoms_params)
scf = SCF(atoms, **scf_params)
print(f'Grid points: {atoms.Ns}')
start = time.perf_counter()
scf.run()
end = time.perf_counter()
print(f'Time: {end - start:.3f} s')
