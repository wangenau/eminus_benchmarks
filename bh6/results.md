# Results

## eminus

MUE:  18.828 kcal/mol
MAE:  18.828 kcal/mol
RMSE: 7.941 kcal/mol

## PySCF

MUE:  18.747 kcal/mol
MAE:  18.747 kcal/mol
RMSE: 7.986 kcal/mol
