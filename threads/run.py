"""Compare FFT operator multiprocessing timings."""
import time

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import default_rng

from eminus import Atoms, config


threads = range(1, 7)
rng = default_rng()

I = []
J = []
Idag = []
Jdag = []

I_torch = []
J_torch = []
Idag_torch = []
Jdag_torch = []

print('Perform real FFTs...')
for t in threads:
    print(f'Threads={t}...')
    config.threads = t

    config.use_torch = False
    atoms = Atoms('Ne', (0, 0, 0), ecut=100).build()
    W_full = rng.standard_normal((len(atoms.G2), atoms.occ.Nstate))
    W_active = rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate))
    t = time.perf_counter()
    atoms.I(W_active)
    I.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag.append(time.perf_counter() - t)

    config.use_torch = True
    t = time.perf_counter()
    atoms.I(W_active)
    I_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag_torch.append(time.perf_counter() - t)

I = np.asarray(I)
J = np.asarray(J)
Idag = np.asarray(Idag)
Jdag = np.asarray(Jdag)

plt.figure()
plt.plot(threads, I / I_torch, label='I')
plt.plot(threads, J / J_torch, label='J')
plt.plot(threads, Idag / Idag_torch, label='Idag')
plt.plot(threads, Jdag / Jdag_torch, label='Jdag')
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Treads')
plt.ylabel(r't$_\mathrm{Scipy}$ / t$_\mathrm{Torch}$')
plt.title('Threads (larger = faster than SciPy)')
plt.legend()
plt.savefig('results_real.png')

plt.figure()
plt.plot(threads, I + J + Idag + Jdag, label='Scipy')
plt.plot(threads, np.asarray(I_torch) + J_torch + Idag_torch + Jdag_torch, label='pytorch')
plt.xlabel('Treads')
plt.ylabel(r'$\Sigma$t$_\mathrm{code}$')
plt.legend()
plt.savefig('results_real_raw.png')

I = []
J = []
Idag = []
Jdag = []

I_torch = []
J_torch = []
Idag_torch = []
Jdag_torch = []

print('Perform complex FFTs...')
for t in threads:
    print(f'Threads={t}...')
    config.threads = t

    config.use_torch = False
    atoms = Atoms('Ne', (0, 0, 0), ecut=100).build()
    W_full = rng.standard_normal((len(atoms.G2), atoms.occ.Nstate)) + 1j * rng.standard_normal((len(atoms.G2), atoms.occ.Nstate))
    W_active = rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate)) + 1j * rng.standard_normal((len(atoms.G2c), atoms.occ.Nstate))
    t = time.perf_counter()
    atoms.I(W_active)
    I.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag.append(time.perf_counter() - t)

    config.use_torch = True
    t = time.perf_counter()
    atoms.I(W_active)
    I_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.J(W_full)
    J_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Idag(W_full)
    Idag_torch.append(time.perf_counter() - t)
    t = time.perf_counter()
    atoms.Jdag(W_active)
    Jdag_torch.append(time.perf_counter() - t)

I = np.asarray(I)
J = np.asarray(J)
Idag = np.asarray(Idag)
Jdag = np.asarray(Jdag)

plt.figure()
plt.plot(threads, I / I_torch, label='I')
plt.plot(threads, J / J_torch, label='J')
plt.plot(threads, Idag / Idag_torch, label='Idag')
plt.plot(threads, Jdag / Jdag_torch, label='Jdag')
plt.axhline(1, c='dimgrey', ls='--')
plt.xlabel('Treads')
plt.ylabel(r't$_\mathrm{Scipy}$ / t$_\mathrm{Torch}$')
plt.title('Threads (larger = faster than SciPy)')
plt.legend()
plt.savefig('results_complex.png')

plt.figure()
plt.plot(threads, I + J + Idag + Jdag, label='Scipy')
plt.plot(threads, np.asarray(I_torch) + J_torch + Idag_torch + Jdag_torch, label='pytorch')
plt.xlabel('Treads')
plt.ylabel(r'$\Sigma$t$_\mathrm{code}$')
plt.legend()
plt.savefig('results_complex_raw.png')
