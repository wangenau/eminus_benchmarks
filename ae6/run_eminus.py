"""Calculate the AE6 benchmark using eminus."""
import numpy as np

from eminus import Atoms, SCF, read
from eminus.units import ha2kcalmol

# Main calculation parameters
atoms_params = {'ecut': 30, 'a': 20, 'verbose': 2}
scf_params = {'xc': 'spw92', 'etol': 1e-6, 'opt': {'auto': 250}}

# Molecules in the AE6 set
molecules = ('C3H4_pro', 'C4H8_cyc', 'HCOCOH', 'S2', 'SiH4', 'SiO')
# Lookup dictionary for the spin of atoms
spin = {'C': 2, 'H': 1, 'O': 2, 'S': 2, 'Si': 2}
# Reference energies
reference = (704.79, 1149.01, 633.35, 101.67, 322.40, 192.08)
# Dictionary to save the energies of atoms
calculated = {}

E_mols = []
E_atoms = []
for im in molecules:
    # Calculate the molecules restricted
    print(f'Calculate {im}...')
    atom, X = read(f'{im}.xyz')
    atoms = Atoms(atom, X, **atoms_params)
    E_mols.append(SCF(atoms, **scf_params).run())

    E_atom = 0
    # Calculate each atom only once
    for ia in set(atoms.atom):
        # If the atom has not been calculated do it unrestricted and add it to the lookup dictionary
        if ia not in calculated:
            print(f'Calculate {ia}...')
            atoms_tmp = Atoms(ia, (0, 0, 0), **atoms_params, spin=spin[ia])
            calculated[ia] = SCF(atoms_tmp, **scf_params).run()
        # Multiply the atom energies with the amount of the atom in the molecule
        E_atom += calculated[ia] * atoms.atom.count(ia)
    E_atoms.append(E_atom)

# Calculate and print the errors
error = reference - ha2kcalmol(E_atoms - np.asarray(E_mols))
mue = np.sum(error) / len(error)
mae = np.sum(np.abs(error)) / len(error)
rmse = np.sqrt(np.sum(error**2)) / len(error)
print(f'MUE:  {mue:.3f} kcal/mol')
print(f'MAE:  {mae:.3f} kcal/mol')
print(f'RMSE: {rmse:.3f} kcal/mol')
