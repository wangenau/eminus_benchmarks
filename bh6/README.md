# BH6 benchmark

The geometries are taken from https://comp.chem.umn.edu/db/dbs/htbh38.html

The reference energies are taken from https://pubs.acs.org/doi/10.1021/jp035287b
