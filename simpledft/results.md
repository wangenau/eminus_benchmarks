# Results

## eminus

Etot(['H']) = -0.438468733681 Eh
57.089 seconds
Etot(['He']) = -2.632046522727 Eh
14.012 seconds
Etot(['H', 'H']) = -1.113986046437 Eh
20.791 seconds

## SimpleDFT

Etot(['H']) = -0.438468733681 Eh
107.080335 seconds
Etot(['He']) = -2.632046522727 Eh
28.426521 seconds
Etot(['H', 'H']) = -1.113986046437 Eh
41.110688 seconds

# SimpleDFT.jl

Etot(["H"]) = -0.438468733681 Eh
125.331976 seconds
Etot(["He"]) = -2.632046522727 Eh
29.530057 seconds
Etot(["H", "H"]) = -1.113986046437 Eh
43.645980
