"""Benzmark using PySCF."""
import time

from pyscf import dft, gto

# Main calculation parameters
atoms_params = {'basis': 'aug-pc-1', 'verbose': 4}
scf_params = {'xc': 'spw92', 'conv_tol': 1e-7, 'max_cycle': 250, 'grid_level': 8}

# Calculate benzene
mol = gto.M(atom='benzene.xyz', **atoms_params)
mf = dft.RKS(mol, xc=scf_params['xc'])
mf.conv_tol = scf_params['conv_tol']
mf.max_cycle = scf_params['max_cycle']
mf.grids.level = scf_params['grid_level']
mf.grids.build()
print(f'Grid points: {len(mf.grids.coords)}')
start = time.perf_counter()
mf.run()
end = time.perf_counter()
print(f'Time: {end - start:.3f} s')
