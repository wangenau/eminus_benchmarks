# Functional family timings

Compare the timings of different functional families.

Use PWmod for LDA, PBE for GGA (PWmod is included in PBE), and TPSS for meta-GGA (PBE is included in TPSS).

Use different parameters for PWDFT.jl since TPSS is not available.
