"""Calculate the BH6 benchmark using PySCF."""
import numpy as np

from eminus.units import ha2kcalmol
from pyscf import dft, gto

# Main calculation parameters
atoms_params = {'basis': 'aug-pc-1', 'verbose': 2}
scf_params = {'xc': 'spw92', 'conv_tol': 1e-6, 'max_cycle': 250, 'grid_level': 3, 'prune': None}

# Systems in the BH6 set
systems = ('CH3', 'CH4', 'H', 'H2', 'H2O', 'H2S', 'HS', 'O', 'OH', 'TS4', 'TS12', 'TS13')
# Lookup dictionary for the spin
spin = {'CH3': 1, 'CH4': 0, 'H': 1, 'H2': 0, 'H2O': 0, 'H2S': 0, 'HS': 1, 'O': 2, 'OH': 1,
        'TS4': 1, 'TS12': 2, 'TS13': 1}
# Reference energies
reference = {'r7': 6.7, 'r8': 19.6, 'r23': 10.7, 'r24': 13.1, 'r25': 3.6, 'r26': 17.3}
# Dictionary to save the energies
calculated = {}

for isys in systems:
    # Calculate the systems
    print(f'Calculate {isys}...')
    mol = gto.M(atom=f'{isys}.xyz', **atoms_params, spin=spin[isys])
    mf = dft.UKS(mol)
    mf.xc = scf_params['xc']
    mf.conv_tol = scf_params['conv_tol']
    mf.max_cycle = scf_params['max_cycle']
    mf.grids.level = scf_params['grid_level']
    mf.grids.prune = scf_params['prune']
    mf.grids.build()
    mf.run()
    if not mf.converged:
        print('WARNING: SCF not converged!')
    calculated[isys] = mf.e_tot

# Calculate and print the errors
error = np.zeros(len(reference))
error[0] = reference['r7'] - ha2kcalmol(calculated['TS4'] - calculated['OH'] - calculated['CH4'])
error[1] = reference['r8'] - ha2kcalmol(calculated['TS4'] - calculated['CH3'] - calculated['H2O'])
error[2] = reference['r23'] - ha2kcalmol(calculated['TS12'] - calculated['OH'] - calculated['H'])
error[3] = reference['r24'] - ha2kcalmol(calculated['TS12'] - calculated['O'] - calculated['H2'])
error[4] = reference['r25'] - ha2kcalmol(calculated['TS13'] - calculated['H'] - calculated['H2S'])
error[5] = reference['r26'] - ha2kcalmol(calculated['TS13'] - calculated['HS'] - calculated['H2'])
mue = np.sum(error) / len(error)
mae = np.sum(np.abs(error)) / len(error)
rmse = np.sqrt(np.sum(error**2)) / len(error)
print(f'MUE:  {mue:.3f} kcal/mol')
print(f'MAE:  {mae:.3f} kcal/mol')
print(f'RMSE: {rmse:.3f} kcal/mol')
