"""Calculate the AE6 benchmark using PySCF."""
import numpy as np

from eminus import read
from eminus.units import ha2kcalmol
from pyscf import dft, gto

# Main calculation parameters
atoms_params = {'basis': 'aug-pc-1', 'verbose': 2}
scf_params = {'xc': 'spw92', 'conv_tol': 1e-6, 'max_cycle': 250, 'grid_level': 3, 'prune': None}

# Molecules in the AE6 set
molecules = ('C3H4_pro', 'C4H8_cyc', 'HCOCOH', 'S2', 'SiH4', 'SiO')
# Lookup dictionary for the spin of atoms
spin = {'C': 2, 'H': 1, 'O': 2, 'S': 2, 'Si': 2}
# Reference energies
reference = (704.79, 1149.01, 633.35, 101.67, 322.40, 192.08)
# Dictionary to save the energies of atoms
calculated = {}

E_mols = []
E_atoms = []
for im in molecules:
    # Calculate the molecules restricted
    print(f'Calculate {im}...')
    mol = gto.M(atom=f'{im}.xyz', **atoms_params)
    mf = dft.RKS(mol, xc=scf_params['xc'])
    mf.conv_tol = scf_params['conv_tol']
    mf.max_cycle = scf_params['max_cycle']
    mf.grids.level = scf_params['grid_level']
    mf.grids.prune = scf_params['prune']
    mf.grids.build()
    mf.run()
    if not mf.converged:
        print('WARNING: SCF not converged!')
    E_mols.append(mf.e_tot)

    E_atom = 0
    # Calculate each atom only once
    atom, _ = read(f'{im}.xyz')
    for ia in set(atom):
        # If the atom has not been calculated do it unrestricted and add it to the lookup dictionary
        if ia not in calculated:
            print(f'Calculate {ia}...')
            mol = gto.M(atom=f'{ia} 0 0 0', **atoms_params, spin=spin[ia])
            mf = dft.UKS(mol, xc=scf_params['xc'])
            mf.conv_tol = scf_params['conv_tol']
            mf.max_cycle = scf_params['max_cycle']
            mf.grids.level = scf_params['grid_level']
            mf.grids.prune = scf_params['prune']
            mf.grids.build()
            mf.run()
            if not mf.converged:
                print('WARNING: SCF not converged!')
            calculated[ia] = mf.e_tot
        # Multiply the atom energies with the amount in the molecule
        E_atom += calculated[ia] * atom.count(ia)
    E_atoms.append(E_atom)

# Calculate and print the errors
error = reference - ha2kcalmol(E_atoms - np.asarray(E_mols))
mue = np.sum(error) / len(error)
mae = np.sum(np.abs(error)) / len(error)
rmse = np.sqrt(np.sum(error**2)) / len(error)
print(f'MUE:  {mue:.3f} kcal/mol')
print(f'MAE:  {mae:.3f} kcal/mol')
print(f'RMSE: {rmse:.3f} kcal/mol')
