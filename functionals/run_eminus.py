"""Calculate a system using different functional families."""
import time

from eminus import Atoms, SCF

# Main calculation parameters
atoms_params = {'ecut': 30, 'a': 20, 'unrestricted': False, 'verbose': 0}
scf_params = {'etol': 1e-7, 'opt': {'pccg': 13}}

# Calculate the functionals
# We do not care if the calculation converges or not, we just want the timings
for ixc in (':1,:13', ':101,:130', ':202,:231'):
    atoms = Atoms('Ne', (0, 0, 0), **atoms_params)
    scf = SCF(atoms, xc=ixc, **scf_params)
    start = time.perf_counter()
    scf.run()
    end = time.perf_counter()
    print(f'{end - start:.6f} seconds')
