"""Analyze the calculation time depending on the plane wave cutoff energy."""
import matplotlib.pyplot as plt
import numpy as np

from eminus import Atoms, SCF


ecut = np.arange(10, 160, 10)
times = []

for e in ecut:
    print(f'Calculating ecut={e}...')
    atoms = Atoms('Ne', (0, 0, 0), ecut=e, verbose=0)
    scf = SCF(atoms, opt={'sd': 10})
    scf.run()
    times.append(scf._opt_log['sd']['time'])

plt.figure()
plt.plot(ecut, times)
plt.xlabel('ecut [$a_0$]')
plt.ylabel('Time [$s$]')
plt.title('Time for 10 sd steps')
plt.savefig('results.png')
