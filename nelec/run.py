"""Analyze the time scaling depending on the number of electrons/states."""
import matplotlib.pyplot as plt
import numpy as np

from eminus import Atoms, SCF


nelec = np.arange(1, 16)
times_rks = []
times_uks = []

print('Calculate RKS...')
for n in nelec:
    print(f'Calculating nelec={n}...')
    atoms = Atoms('He', (0, 0, 0), verbose=0, unrestricted=False)
    atoms.f = np.ones(n)
    scf = SCF(atoms, opt={'sd': 10})
    scf.run()
    times_rks.append(scf._opt_log['sd']['time'])

print('Calculate UKS...')
for n in nelec:
    print(f'Calculating nelec={n}...')
    atoms = Atoms('He', (0, 0, 0), verbose=0, unrestricted=True)
    f = np.zeros((2, n))
    f[0] += 1
    atoms.f = f
    scf = SCF(atoms, opt={'sd': 10})
    scf.run()
    times_uks.append(scf._opt_log['sd']['time'])

fig, ax1 = plt.subplots()
ax1.set_xlabel('Number of electrons')
ax1.set_ylabel('Time (RKS) [$s$]', c='blue')
ax1.plot(nelec, times_rks, label='RKS', c='blue')
ax2 = ax1.twinx()
ax2.set_ylabel('Time (UKS) [$s$]', c='orange')
ax2.plot(nelec, times_uks, label='UKS', c='orange')
plt.title('Time for 10 sd steps')
fig.savefig('results.png')
