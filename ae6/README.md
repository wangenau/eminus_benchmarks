# AE6 benchmark

The geometries are taken from https://comp.chem.umn.edu/db/dbs/mgae109.html

The reference energies are taken from https://pubs.acs.org/doi/10.1021/jp035287b
